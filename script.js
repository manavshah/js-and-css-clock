const secondHand = document.querySelector(".second-hand");
const minHand = document.querySelector(".min-hand");
const hourHand = document.querySelector(".hour-hand");

function calculatingTime(){
    var d = new Date();
    // console.log(d);

    const seconds = d.getSeconds();
    const secondDeg = ((seconds / 60) * 360) + 90;
    secondHand.style.transform = `rotate(${secondDeg}deg)`;
    
    const mins = d.getMinutes();
    console.log(mins);
    // const minDeg = (((mins / 60) * 360) 
    const minDeg = (mins % 60) + 90;
    minHand.style.transform = `rotate(${minDeg}deg)`;

    const hour = d.getHours();
    const hourDeg = Math.floor(hour / 60);
    console.log(hourDeg);
    hourHand.style.transform = `rotate(${hourDeg}deg)`; 

}
setInterval(calculatingTime, 1000);

calculatingTime();